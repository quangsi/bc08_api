function fetchProductList() {
  startLoading();
  axios({
    url: "https://633ec05b0dbc3309f3bc5455.mockapi.io/product",
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      renderProductList(res.data);
      endLoading();
    })
    .catch(function (err) {
      endLoading();

      console.log(err);
    });
}
// gọi lần đầu khi load trang
fetchProductList();

function deleteProduct(id) {
  startLoading();
  axios({
    url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/product/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      fetchProductList();
    })
    .catch(function (err) {
      endLoading();
    });
}

function addProduct() {
  var product = getDataForm();
  axios({
    url: "https://633ec05b0dbc3309f3bc5455.mockapi.io/product",
    method: "POST",
    data: product,
  })
    .then(function (res) {
      console.log(res);
      $("#myModal").modal("hide");
      fetchProductList();
    })
    .catch(function (err) {
      console.log(err);
    });
}
// lodash

function editProduct(id) {
  $("#myModal").modal("show");
  axios({
    url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/product/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      showDataForm(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}

function updateProduct() {
  var product = getDataForm();
  axios({
    url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/product/${product.id}`,
    method: "PUT",
    data: product,
  })
    .then(function (res) {
      $("#myModal").modal("hide");
      fetchProductList();
    })
    .catch(function (err) {
      console.log(err);
    });
}
