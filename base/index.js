//  đồng bộ ,bất đồng bộ

// setTimeOut, axios ~ bất đồng bộ ~ chạy sau cùng sau khi __tất cả__ code đồng bộ chạy xong

setTimeout(function () {
  console.log("bật quảng cáo 1");
}, 1000);
setTimeout(function () {
  console.log("bật quảng cáo 2");
}, 500);
console.log(10);
console.log(9);

// let test = axios();
// console.log("😀 - test", test);

axios({
  url: "https://api.tiki.vn/raiden/v2/menu-config?platform=desktop",
  method: "GET",
})
  .then(function (res) {
    // thành công
    console.log("😀 - .then - res", res.data.highlight_block.items);
  })
  .catch(function (err) {
    // thất bại
    console.log("😀 - err", err);
  });
